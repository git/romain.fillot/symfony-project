<?php

namespace App\Service;

use App\Entity\Twok;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class TwokManager{

    public function getTwokByID(int $id): Twok{
        $data = $this->getAllTwork();

        foreach ($data as $ligne) {
            if ($ligne['id'] == $id) {
                $twok = new Twok($ligne['id'], $ligne['author'], $ligne['content'], $ligne['created_at']);
                return $twok;
                break; 
            }
        }

        return new Twok(0, '', '', '');
    }

    public function addTwok(Twok $my_twok) {
        $encoders = [new XmlEncoder(), new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        
        $path = '../var/test.json';
        var_dump($my_twok);
        $jsonContent = $serializer->serialize($my_twok, 'json');
        // $jsonString = json_encode($my_twok);
        file_put_contents($path, $jsonContent);
    }

    public function getAllTwork(): array{
        $path = '../var/twoks_db.json';
        $jsonString = file_get_contents($path);
        $jsonData = json_decode($jsonString, true);
        return $jsonData;
    }

}
