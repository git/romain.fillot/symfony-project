<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use App\Service\TwokManager;
use Psr\Log\LoggerInterface;
use App\Entity\Twok;

class TweetTokController extends AbstractController
{
    #[Route('/home', name: 'app_tweet_tok')]
    public function home(TwokManager $twok_manager): Response
    {
        $twok = $twok_manager->getTwokByID(37497);

        return $this->render('tweet_tok/index.html.twig', [
            'controller_name' => 'TweetTokController',
            'author'=> $twok->getAuteur(),
            'content' => $twok->getMessage()
        ]);
    }

    #[Route('/home/twok', name: 'add_twok')]
    public function addTwok(TwokManager $twok_manager): Response
    {
        $twok = new Twok(10, 'romain', 'L\'instant présent est le plus important. Ne perdons pas notre temps
        avec le passé et le futur', '09/02/2024 10:30:33');
        $twok = $twok_manager->addTwok($twok);
        

        return $this->render('tweet_tok/index.html.twig', [
            'controller_name' => 'TweetTokController',
            'author'=> 'test',
            'content' => 'test'
        ]);
    }
}
